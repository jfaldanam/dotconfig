#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

########################################################################
#                                                                      #
#           PS1 and git prompt                                         #
#                                                                      #
########################################################################
# if .git-prompt.sh exists, configure and execute it
# Source: https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh
if [ -f ~/.git-prompt.sh ]; then
  GIT_PS1_SHOWDIRTYSTATE=false
  GIT_PS1_SHOWSTASHSTATE=false
  GIT_PS1_SHOWUNTRACKEDFILES=false
  GIT_PS1_SHOWUPSTREAM="auto"
  GIT_PS1_HIDE_IF_PWD_IGNORED=false
  GIT_PS1_SHOWCOLORHINTS=true

  # Load git prompt script
  source ~/.git-prompt.sh
fi

# Return part of a PS1 prompt showing current python virtualenvs
# Source: https://stackoverflow.com/a/14987716
get_venv_info() {
  if [ -z "$VIRTUAL_ENV_DISABLE_PROMPT" ] ; then
    if [ "`basename \"$VIRTUAL_ENV\"`" = "__" ] ; then
      # special case for Aspen magic directories
      # see http://www.zetadev.com/software/aspen/
      VENV="[`basename \`dirname \"$VIRTUAL_ENV\"\``] "
    elif [ "$VIRTUAL_ENV" != "" ]; then
      VENV="(`basename \"$VIRTUAL_ENV\"`) "
    fi
  fi

  echo "$VENV"
}

custom_prompt() {
  # Define colors
  local RESET="\[\033[m\]"           # reset
  local EMRESET="\[\033[1m\]"        # reset with emphasis

  # regular colors
  local BLACK="\[\033[0;30m\]"       # black
  local RED="\[\033[0;31m\]"         # red
  local GREEN="\[\033[0;32m\]"       # green
  local YELLOW="\[\033[0;33m\]"      # yellow
  local BLUE="\[\033[0;34m\]"        # blue
  local MAGENTA="\[\033[0;35m\]"     # magenta
  local CYAN="\[\033[0;36m\]"        # cyan
  local LIGHTGRAY="\[\033[0;37m\]"   # light gray

  # emphasized (bolded) colors
  local EMBLACKK="\[\033[1;30m\]"    # black        bold
  local EMRED="\[\033[1;31m\]"       # red          bold
  local EMGREEN="\[\033[1;32m\]"     # green        bold
  local EMYELLOW="\[\033[1;33m\]"    # yellow       bold
  local EMBLUE="\[\033[1;34m\]"      # blue         bold
  local EMMAGENTA="\[\033[1;35m\]"   # magenta      bold
  local EMCYAN="\[\033[1;36m\]"      # cyan         bold
  local EMLIGHTGRAY="\[\033[1;37m\]" # light gray   bold

  # background colors
  local BGBLACK="\[\033[40m\]"       # black      background
  local BGRED="\[\033[41m\]"         # red        background
  local BGGREEN="\[\033[42m\]"       # green      background
  local BGYELLOW="\[\033[43m\]"      # yellow     background
  local BGBLUE="\[\033[44m\]"        # blue       background
  local BGMAGENTA="\[\033[45m\]"     # magenta    backgrund
  local BGCYAN="\[\033[46m\]"        # cyan       background
  local BGLIGHTGRAY="\[\033[47m\]"   # light gray background

  # Set user and root colors
  local UC="$RESET$EMRESET"            # user's color
  [ $UID -eq "0" ] && UC="$RED"        # root's color

  # Split PS1 in readable parts
  local __venv="$EMGREEN$(get_venv_info)"
  local __user="$UC\u"
  local __at="$EMYELLOW@"
  local __host="$CYAN\h"
  local __cwd="$EMYELLOW\W"            # capital 'W': current directory, small 'w': full file path
  local __prompt_tail="$MAGENTA\$"
  local __user_input_color="$RESET"
  local __git_branch="$EMGREEN$(__git_ps1)"

  # Build the PS1 (Prompt String)
  PS1="$__venv$__user$__at$__host $__cwd$__git_branch $__prompt_tail$__user_input_color "
}

########################################################################
#                                                                      #
#           bash completions                                           #
#                                                                      #
########################################################################

# if bash_completions exists, source it
# Source: https://github.com/scop/bash-completion
if [ -f /usr/share/bash-completion/bash_completion ]; then
  # Load bash completions script
  source /usr/share/bash-completion/bash_completion
fi

for FILE in ~/.bash_completion/*; do
  if [ -f "$FILE" ]; then
    source "$FILE"
  fi
done

# If exists, install fzf bindings
if [ -f /usr/share/doc/fzf/examples/key-bindings.bash ]; then
  source /usr/share/doc/fzf/examples/key-bindings.bash
fi

########################################################################
#                                                                      #
#           alias                                                      #
#                                                                      #
########################################################################

# Color all the things
alias ls='ls --color=auto'
alias ip='ip -color'
alias diff='diff --color'

# Management of my dotconfig bare repository
alias dotconfig='/usr/bin/git --git-dir=$HOME/.dotconfig/ --work-tree=$HOME'

# Search pacman database for a file
alias where='pacman -Fx'

# Show stats with random ascii image
alias neofetch='neofetch --source $(ls $HOME/.config/neofetch/ascii-*.txt | shuf -n 1)'

# ¯\_(ツ)_/¯
alias shrug='echo "¯\_(ツ)_/¯"'

# GIF shortcuts (mainly ascii.live[https://github.com/hugomd/ascii-live])
alias parrot='curl parrot.live'
alias PARTYUP=parrot

alias nyan='curl ascii.live/nyan'

alias rickroll='curl ascii.live/rick'

########################################################################
#                                                                      #
#           functions                                                  #
#                                                                      #
########################################################################

# Import tmux sessions
source ~/.tmux-sessions.sh

# Quick "backup" (Warning!!!! This is not a proper backup)
function backup {
  cp --preserve --update $1 $1.bckup
}

# Original: https://old.reddit.com/r/bash/comments/rmvqe7/cd_for_the_fzf_age/hpomn3k/
function cd {
    if [ -d "$@" ] || \
           [[ "$@" =~ ^-$|^\.{1,2}$ ]] || \
           [[ "$@" == $HOME ]] ; then
        builtin cd "$@"
    else
        if [[ "$@" = /* ]]; then
          CONTEXT_PATH="/"
        else
          CONTEXT_PATH="."
        fi

        CD_PATH=$(find $CONTEXT_PATH -type d 2>/dev/null | fzf +m --query "$@")
        if [[ -z "$CD_PATH" ]]; then
          echo "Cancelled"
          builtin cd $CD_PREVIOUS
          return -1
        fi

        builtin cd $CD_PATH
    fi
}

########################################################################
#                                                                      #
#           enviroment variables                                       #
#                                                                      #
########################################################################

# Set up PS1 through PROMPT_COMMAND
export PROMPT_COMMAND=custom_prompt

# PATH additions
export PATH=$PATH:$HOME/.poetry/bin/

# GPG through tty
export GPG_TTY=$(tty)

# Automatically update history
# Source:https://web.archive.org/web/20090815205011/http://www.cuberick.com/2008/11/update-bash-history-in-realtime.html
shopt -s histappend
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"

########################################################################
#                                                                      #
#           executions                                                 #
#                                                                      #
########################################################################

# Source local overrides
if [ -f $HOME/.bashrc.local ]; then
  source $HOME/.bashrc.local
fi

neofetch
