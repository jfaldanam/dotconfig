# dotconfig

## Set up

* Set up alias: `alias dotconfig='/usr/bin/git --git-dir=$HOME/.dotconfig/ --work-tree=$HOME'`

* Clone the repo: `git clone --bare git@gitlab.com:jfaldanam/dotconfig.git $HOME/.dotconfig`

* Checkout the content of the bare repository: `dotconfig checkout`

If it fails delete or backup the conflicted files.

* Set the showUntrackedFiles flag: `dotconfig config --local status.showUntrackedFiles no`

## Automatic installation
You can find a snippet for the automatic instalation [here.](jfaldanam/dotconfig$1971668)

## Manual patches

* vscodium: [Installation of extensions from Microsoft extension](https://github.com/microsoft/vscode/issues/31168#issuecomment-317319063). The route of the product file in archlinux can be found with `$ pacman -Ql vscodium-bin | grep product.json`, may need to be changed if the vscodium-git package is used. On Ubuntu variants `dpkg -L codium | grep product.json`. Additionally, I am currently monitor the effect of this patch ["keyboard.dispatch": "keyCode"](https://github.com/Microsoft/vscode/wiki/Keybinding-Issues#troubleshoot-linux-keybindings), as it fixes common issues when using remote desktop, but I am unsure of its secondary effects.

* Dependencies:
  * [git-prompt](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh): Fancy prompt with current git repository.
  * [fzf](https://github.com/junegunn/fzf): Fuzzy bash keybinds, fancy cd.
