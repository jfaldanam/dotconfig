#!/bin/bash

function alltop {
# Starts a tmux SESSION for monitoring system resources and gpu
  SESSION="resource-monitor"

  # If session doesn't exist, create it.
  if tmux has-session -t $SESSION 2>/dev/null; then
    tmux new-session -d -s $SESSION

    # Default to btop, fallback to htop if not installed
    MONITORING_PROGRAM="btop"
    if ! command -v $MONITORING_PROGRAM &> /dev/null; then
      MONITORING_PROGRAM="htop"
    fi

    # Create window panel, 75% for main resource monitoring, 25% for GPU
    tmux send-keys -t $SESSION:0 $MONITORING_PROGRAM C-m
    tmux split-window -t $SESSION:0 -h -p 25 "watch -n 0.5 nvidia-smi"
  fi

  #Attach to session
  tmux attach -t $SESSION
}